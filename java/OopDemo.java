/**
 * @author 郑洪耕
 *
 * @date 2021-10-27
 */
public class OopDemo {

    public static void main(String[] args) {
        IAmChildren children = new IAmChildren();
        System.out.printf("getPublicProperty: %s\n", children.getPublicProperty());
        System.out.printf("getProtectedProperty: %s\n", children.getProtectedProperty());
    }

}
