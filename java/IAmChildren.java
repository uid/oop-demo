/**
 * @author 郑洪耕
 *
 * @date 2021-10-27
 */
public class IAmChildren extends IAmParent {

    public String publicProperty = "Children public property";

    protected String protectedProperty = "Children protected property";

}
