/**
 * @author 郑洪耕
 *
 * @date2021-10-27
 */
public class IAmParent {

    public String publicProperty = "Parent public property";

    protected String protectedProperty = "Parent protected property";

    public String getPublicProperty() {
        return publicProperty;
    }

    public String getProtectedProperty() {
        return protectedProperty;
    }

}
