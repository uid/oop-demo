#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import IAmParent

'''
 * @author 郑洪耕
 *
 * @date 2021-10-27
'''
class IAmChildren(IAmParent.IAmParent):

    def __init__(self):
        self.publicProperty = "Children public property";
