#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
 * @author 郑洪耕
 *
 * @date 2021-10-27
'''
class IAmParent(object):

    def __init__(self):
        self.publicProperty = "Parent public property";

    def getPublicProperty(self):
        return self.publicProperty;
