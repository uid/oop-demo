<?php

/**
 * @author 郑洪耕
 *
 * @date2021-10-27
 */
class IAmParent {

    public $publicProperty = "Parent public property";

    protected $protectedProperty = "Parent protected property";

    public function getPublicProperty() {
        return $this->publicProperty;
    }

    public function getProtectedProperty() {
        return $this->protectedProperty;
    }

}
