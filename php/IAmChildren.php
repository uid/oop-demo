<?php

include './IAmParent.php';

/**
 * @author 郑洪耕
 *
 * @date 2021-10-27
 */
class IAmChildren extends IAmParent {

    public $publicProperty = "Children public property";

    protected $protectedProperty = "Children protected property";

}
